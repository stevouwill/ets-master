package com.ease.client.ets.remote;

import android.content.Context;
import android.util.Log;

import com.ease.client.ets.BuildConfig;
import com.ease.client.ets.model.Login;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ETSClientRESTAPI {

  String BASE_URL = "http://52.28.173.172/api/v2/";

  //@GET("your_endpoint") Call<YOUR_POJO> getWeather(@Query("from") String from);

  @FormUrlEncoded
  @POST("auth/login")
  Call<Login> authenticate(@Field("username") String username, @Field("password") String password);


  class Factory {
    private static ETSClientRESTAPI service;

    public static ETSClientRESTAPI getIstance(Context context) {
      if (service == null) {

        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(15, TimeUnit.SECONDS);
        builder.connectTimeout(10, TimeUnit.SECONDS);
        builder.writeTimeout(10, TimeUnit.SECONDS);
       /* builder.interceptors().add(new Interceptor() {
          @Override
          public Response intercept(Chain chain) throws IOException {
            Response response = chain.proceed(chain.request());

            Log.e("client", response.toString());
            Log.e("client", response.body().string());
            
            //return response.body();
            return onOnIntercept(chain);
          }

          private Response onOnIntercept(Chain chain) throws IOException {
            try {
              Response response = chain.proceed(chain.request());
              //String content = UtilityMethods.convertResponseToString(response);
              String content =response.toString();
              Log.d(TAG, lastCalledMethodName + " - " + content);
              return response.newBuilder().body(ResponseBody.create(response.body().contentType(), content)).build();
            }
            catch (SocketTimeoutException exception) {
              exception.printStackTrace();
              if(listener != null)
                listener.onConnectionTimeout();
            }

            return chain.proceed(chain.request());
          }
        });*/

        //builder.certificatePinner(new CertificatePinner.Builder().add("*.androidadvance.com", "sha256/RqzElicVPA6LkKm9HblOvNOUqWmD+4zNXcRb+WjcaAE=")
        //    .add("*.xxxxxx.com", "sha256/8Rw90Ej3Ttt8RRkrg+WYDS9n7IS03bk5bjP/UXPtaY8=")
        //    .add("*.xxxxxxx.com", "sha256/Ko8tivDrEjiY90yGasP6ZpBU4jwXvHqVvQI0GS3GNdA=")
        //    .add("*.xxxxxxx.com", "sha256/VjLZe/p3W/PJnd6lL8JVNBCGQBZynFLdZSTIqcO0SJ8=")
        //    .build());

        if (BuildConfig.DEBUG) {
          HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
          interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
          builder.addInterceptor(interceptor);
        }

        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(context.getCacheDir(), cacheSize);
        builder.cache(cache);

        //Retrofit retrofit = new Retrofit.Builder().client(builder.build()).addConverterFactory(GsonConverterFactory.create()).baseUrl(BASE_URL).addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create()).build();
        //service = retrofit.create(ETSClientRESTAPI.class);

          service =  new Retrofit.Builder()
                  .baseUrl(BASE_URL)
                  .client(builder.build())
                  .addConverterFactory(GsonConverterFactory.create(new Gson()))
                  //.addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                  .build()
                  .create(ETSClientRESTAPI.class);

        return service;
      } else {
        return service;
      }
    }

    /*String getErrorMessage(Throwable e) {
      RetrofitError retrofitError;
      if (e instanceof RetrofitError) {
        retrofitError = ((RetrofitError) e);
        if (retrofitError.getKind() == RetrofitError.Kind.NETWORK) {
          return "Network is down!";
        }
      }
    }*/

  }
}
