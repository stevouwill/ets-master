package com.ease.client.ets;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;

/**
 * Created by williams on 16/09/2016.
 */
public class GenericException<T> {

    private static DialogFactory dialogFactory;
    Class theClass = null;
    public GenericException(Class theClass) {
        this.theClass = theClass;
        dialogFactory=new DialogFactory();
    }
    public T createInstance()
            throws IllegalAccessException, InstantiationException {
        return (T) this.theClass.newInstance();
    }


    public static void onResponseError(int code,Context context, View rootView){
       if(code==401){
           dialogFactory.showAlertDialogWithHomepage(context, "connexion failed", "Invalid username or password", false);
           Log.e("401", "Invalid username or password");
          //dialogFactory.createSimpleOkDialog(context, "connexion failed", "Invalid username or password");
           //dialogFactory.showErrorSnackBar(context,rootView,"Invalid username or password");
       }else if(code==500){
           //dialogFactory.showAlertDialogWithHomepage(context, "error", response.raw().toString());
           Log.e("500", "Invalid username or password");
          //dialogFactory.createGenericErrorDialog(context,"Invalid username or password");
           dialogFactory.showAlertDialogWithHomepage(context, "connexion failed", "Internal error",false);

       }else if(code==302){
           //dialogFactory.showAlertDialogWithHomepage(context, "error", response.raw().toString());
           Log.e("500", "Invalid username or password");
           //dialogFactory.createGenericErrorDialog(context,"Invalid username or password");
           dialogFactory.showAlertDialogWithHomepage(context, "connexion failed", "Internal error",false);

       }

    }
}
