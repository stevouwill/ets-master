package com.ease.client.ets.remote;

/**
 * Created by williams on 09/09/2016.
 */
public class ApiUrl {

    private String PostLogin;
    private String SaveUser;
    private String LoadCities;
    private String GetPrices;
    private String GetDevises;
    private String GetListeCourses;
    private String PostReservation;
    private String EditUser;
    private String DeleteCourses;
    private String RefreshToken;
    private String GetUserInfo;
    private String EditCourse;
    private String GetListeVille;
    private String GetMADPrices;
    private String SaveLocation;
    private String GetLocation;
    private String EditLocation;
    private String DeleteLocation;
    private String GetConforts;



    public String getPostLogin() {
        return PostLogin;
    }

    public String getSaveUser() {
        return SaveUser;
    }

    public String getLoadCities() {
        return LoadCities;
    }

    public String getGetPrices() {
        return GetPrices;
    }

    public String getGetDevises() {
        return GetDevises;
    }

    public String getGetListeCourses() {
        return GetListeCourses;
    }

    public String getPostReservation() {
        return PostReservation;
    }

    public String getEditUser() {
        return EditUser;
    }

    public String getDeleteCourses() {
        return DeleteCourses;
    }

    public String getRefreshToken() {
        return RefreshToken;
    }

    public String getGetUserInfo() {
        return GetUserInfo;
    }

    public String getEditCourse() {
        return EditCourse;
    }

    public String getGetListeVille() {
        return GetListeVille;
    }

    public String getGetMADPrices() {
        return GetMADPrices;
    }

    public String getSaveLocation() {
        return SaveLocation;
    }

    public String getGetLocation() {
        return GetLocation;
    }

    public String getEditLocation() {
        return EditLocation;
    }

    public String getDeleteLocation() {
        return DeleteLocation;
    }

    public String getGetConforts() {
        return GetConforts;
    }

    public ApiUrl() {
        this.PostLogin = "http://52.28.173.172/api/v2/auth/login";
        this.SaveUser = "http://52.28.173.172/api/v2/user/store";
        this.LoadCities = "http://52.28.173.172/api/v2/booking/cities";
        this.GetPrices = "http://52.28.173.172/api/v2/booking/prices";
        this.GetDevises = "http://52.28.173.172/api/v2/currency";
        this.GetListeCourses = "http://52.28.173.172/api/v2/user/bookings";
        this.PostReservation = "http://52.28.173.172/api/v2/booking/store/";
        this.EditUser = "http://52.28.173.172/api/v2/user/edit";
        this.DeleteCourses = "http://52.28.173.172/api/v2/booking/delete/";
        this.RefreshToken = "http://52.28.173.172/api/v2/token/refresh";
        this.GetUserInfo = "http://52.28.173.172/api/v2/user/load/";
        this.EditCourse = "http://52.28.173.172/api/v2/booking/edit";
        this.GetListeVille = "http://52.28.173.172/api/v2/renting/cities";
        this.GetMADPrices = "http://52.28.173.172/api/v2/renting/prices/";
        this.SaveLocation = "http://52.28.173.172/api/v2/renting/store";
        this.GetLocation = "http://52.28.173.172/api/v2/user/rentings";
        this.EditLocation = "http://52.28.173.172/api/v2/renting/edit";
        this.DeleteLocation = "http://52.28.173.172/api/v2/renting/delete";
        this.GetConforts = "http://52.28.173.172/api/v2/conforts";
    }

}
