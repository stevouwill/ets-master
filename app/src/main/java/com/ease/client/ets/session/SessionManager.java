package com.ease.client.ets.session;

import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.ease.client.ets.SignInActivity;

/**
 * Created by williams on 07/09/2016.
 */
public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "EtsClientPref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "name";

    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";

    //User Token (make variable public to access from outside)
    public static final String KEY_TOKEN="token";

    //User id (make variable public to access from outside)
    public static final String KEY_ID="1";

    //User phone (make variable public to access from outside)
    public static final String KEY_PHONE="phone";

    //User surname (make variable public to access from outside)
    public static final String KEY_SURNAME="surname";

    //User title (make variable public to access from outside)
    public static final String KEY_TITLE="title";

    //

    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     * */
    public void createLoginSession(String name, String email){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing name in pref
        editor.putString(KEY_NAME, name);

        // Storing email in pref
        editor.putString(KEY_EMAIL, email);

        // commit changes
        editor.commit();
    }

    public void createUserSession(String token, String id, String phone, String email, String name, String surname, String title){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing name in pref
        editor.putString(KEY_NAME, name);

        // Storing email in pref
        editor.putString(KEY_EMAIL, email);

        //Storing token in pref
        editor.putString(KEY_TOKEN,token);

        //Storing token in pref
        editor.putString(KEY_ID, id);

        //Storing phone in pref
        editor.putString(KEY_PHONE, phone);

        //Storing surname in pref
        editor.putString(KEY_SURNAME,surname);

        //Storing title in pref
        editor.putString(KEY_TITLE,title);

        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, SignInActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }



    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));

        // user email id
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));

        //user token ;
        user.put(KEY_TOKEN, pref.getString(KEY_TOKEN, null));

        //user id
       // editor.putInt(String.valueOf(KEY_ID), Integer.valueOf(id));
        user.put(KEY_ID, pref.getString(KEY_ID, "1"));

        //user phone
        user.put(KEY_PHONE, pref.getString(KEY_PHONE, null));

        //user surname
        user.put(KEY_SURNAME, pref.getString(KEY_SURNAME, null));

        //user title
        user.put(KEY_TITLE, pref.getString(KEY_TITLE, null));

        // return user
        return user;
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, SignInActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}
