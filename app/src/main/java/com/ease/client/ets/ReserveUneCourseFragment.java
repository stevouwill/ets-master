package com.ease.client.ets;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReserveUneCourseFragment.OnCourseFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReserveUneCourseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReserveUneCourseFragment extends RootFragment implements OnMapReadyCallback, View.OnClickListener {

    @BindView(R.id.sv_destination)
    SearchView mSearchView;
    @BindView(R.id.btn_reserve_la_course)
    Button mBtnReserve;

    private OnCourseFragmentInteractionListener mListener;

    public ReserveUneCourseFragment() {
        // Required empty public constructor
    }


    public static ReserveUneCourseFragment newInstance() {
        ReserveUneCourseFragment fragment = new ReserveUneCourseFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reserve_une_course, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        ButterKnife.bind(this, view);

        FragmentManager fm = getChildFragmentManager();
        SupportMapFragment supportMapFragment =  SupportMapFragment.newInstance();
        if (fm.findFragmentByTag(ETSConstants.TAG_MAP) == null) {
            fm.beginTransaction().add(R.id.map_fragment_container, supportMapFragment, ETSConstants.TAG_MAP).commit();
        }

        mSearchView.setQueryHint("Destination");

        if (supportMapFragment != null) {
            supportMapFragment.getMapAsync(this);
        }

        mBtnReserve.setOnClickListener(this);

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {

        getActivity().setTitle("Reserve Une Course");
        super.onResume();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onFragmentInteraction();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCourseFragmentInteractionListener) {
            mListener = (OnCourseFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.btn_reserve_la_course) {
            getFragmentManager().beginTransaction().replace(R.id.fragment_container, CourseFragment.newInstance(), ETSConstants.TAG_COURSE)
                    .addToBackStack(null)
                    .commit();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnCourseFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction();
    }
}
