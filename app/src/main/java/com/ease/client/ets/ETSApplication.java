package com.ease.client.ets;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Shazi on 22-Aug-16.
 */
public class ETSApplication extends Application{

    @Override
    public void onCreate() {

        Fabric.with(this, new Crashlytics());
        super.onCreate();
    }
}
