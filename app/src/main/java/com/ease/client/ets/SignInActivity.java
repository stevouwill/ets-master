package com.ease.client.ets;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ease.client.ets.model.Login;
import com.ease.client.ets.model.RestError;
import com.ease.client.ets.model.User;
import com.ease.client.ets.remote.ETSClientRESTAPI;
import com.ease.client.ets.remote.RetrofitException;
import com.ease.client.ets.session.SessionManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.SocketTimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.POST;

public class SignInActivity extends BaseActivity implements View.OnClickListener{

    @BindView(R.id.et_email)
    EditText mEtEmail;
    @BindView(R.id.et_password)
    EditText mEtPassword;
    @BindView(R.id.btn_sign_in)
    Button mBtnSignIn;
    @BindView(R.id.txt_sign_up)
    TextView mTxtSignUp;
    @BindView(R.id.text_input_layout_email)
    TextInputLayout mLayoutEmail;
    @BindView(R.id.text_input_layout_password)
    TextInputLayout mLayoutPassword;

    Context context;
    DialogFactory dialogFactory;

    User user;
    String name;
    String email;
    String phone;
    String surname;
    String title;
    String iduser;
    String token;

    SessionManager session;
    Login ExceptionPost;
    GenericException<Login> GenericLogin;
    View rootView;

    private static final String TAG = SignInActivity.class.getSimpleName();

    public static void signIn(Context context) {
        Intent intent = new Intent(context, SignInActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        dialogFactory=new DialogFactory();

        context=this;

        ButterKnife.bind(this);

        ETSUtils.hideSoftKeyboard(this);
        setSignUpClickable();

        mBtnSignIn.setOnClickListener(this);
        mEtEmail.addTextChangedListener(new SignInTextWatcher(mEtEmail));
        mEtPassword.addTextChangedListener(new SignInTextWatcher(mEtPassword));

        // Session Manager
        session = new SessionManager(getApplicationContext());

        //GenericException
        GenericLogin= new GenericException<Login>(Login.class);
        /*try {
            ExceptionPost = GenericLogin.createInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            Log.e("SignInActivity1", e.getMessage());
        } catch (InstantiationException e) {
            e.printStackTrace();
            Log.e("SignInActivity2", e.getMessage());
        }*/
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        rootView=parent;
        return super.onCreateView(parent, name, context, attrs);
    }

    public void setSignUpClickable() {
        String text = getString(R.string.vous_n_avez_pas_de_compte_inscrivez_vous);
        SpannableString ss = new SpannableString(text);
        ClickableSpan span1 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                SignUpActivity.createAccount(SignInActivity.this);
            }
        };

        ss.setSpan(span1, text.indexOf('?') + 1, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new StyleSpan(Typeface.BOLD), text.indexOf('?')+1, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mTxtSignUp.setText(ss);
        mTxtSignUp.setMovementMethod(LinkMovementMethod.getInstance());
    }



    /**
     * Validating form
     */
    private void submitForm() {
        if (!ETSUtils.validateEmail(mEtEmail, mLayoutEmail, this)) {
            return;
        } else {
            mLayoutEmail.setError(null);
            mLayoutEmail.setErrorEnabled(false);
        }

        if (!ETSUtils.validateField(mEtPassword, mLayoutPassword, this)) {
            return;
        } else {
            mLayoutPassword.setError(null);
            mLayoutPassword.setErrorEnabled(false);
        }

        ETSClientRESTAPI etsClientRESTAPI = ETSClientRESTAPI.Factory.getIstance(this);

        etsClientRESTAPI.authenticate(mEtEmail.getText().toString(), mEtPassword.getText().toString())
                        .enqueue(new Callback<Login>() {
                            @Override
                            public void onResponse(Call<Login> call, Response<Login> response) {

                                if (response!=null&&response.isSuccess()&&response.errorBody()==null) {

                                    Log.e("token",response.body().getValue().getToken().toString());
                                    Log.e("user", response.body().getValue().getUser().toString());
                                   // dialogFactory.showAlertDialogWithHomepage(context, "success", response.toString());
                                    Toast.makeText(getApplicationContext(), response.body().getValue().getUser().toString(), Toast.LENGTH_SHORT).show();

                                    user=response.body().getValue().getUser();

                                    token=response.body().getValue().getToken();
                                    name=user.getName();
                                    iduser=user.getId();
                                    email=user.getEmail();
                                    phone=user.getPhone();
                                    surname=user.getSurname();
                                    title=user.getTitle();

                                    session.createUserSession(token, iduser, phone, email, name, surname, title);

                                    // Toast.makeText(getBaseContext(), name, Toast.LENGTH_LONG).show();

                                    dialogFactory.createProgressDialog(context,"please wait");
                                   // HomeActivity.showHomeScreen(context);


                                } else if(response!=null&&response.errorBody()!=null&& !response.isSuccess()){
                                        try {
                                             Log.e(TAG, String.valueOf(response.code()));
                                              String ErrorObject=response.errorBody().string();

                                              try {
                                                  JSONObject error= new JSONObject(ErrorObject);
                                                 // Toast.makeText(getApplicationContext(), error.getString("value"), Toast.LENGTH_SHORT).show();
                                                  dialogFactory.createGenericAlert(context, "connexion failed", error.getString("value"), response.code());
                                              } catch (JSONException e) {
                                                  e.printStackTrace();
                                                  Log.e(TAG, e.getMessage());
                                              }

                                        } catch (IOException e) {
                                            e.printStackTrace();
                                            Log.e(TAG, e.getMessage());
                                        }
                                        //Log.e("succes2", response.errorBody().string().toString());
                                        //ExceptionPost.o
                                      //  GenericLogin.onResponseError(response.code(),context,rootView);
                                    }
                            }



                            public void onFailure(Call<Login> call, Throwable t) {

                               // RetrofitException error = (RetrofitException) t;
                               // Log.e("retrofit", String.valueOf(error.getResponse().errorBody()));
                                //RestError response = error.getBodyAs(RestError.class);
                                ResponseBody message;
                               /* if(t instanceof SocketTimeoutException){
                                    message = "Socket Time out. Please try again.";
                                    Log.e("failure",  message);
                                }*/


                                if(t instanceof HttpException){
                                    message= ((HttpException) t).response().errorBody();
                                   // message = "Socket Time out. Please try again.";
                                    Log.e("failure",  message.toString());
                                    try {
                                        Log.e("failure",  message.string());
                                        Log.e("failure",  message.toString());
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                        Log.e("failureZ", e.getMessage());
                                    }
                                }

                               // Log.e("failure", "failed");
//                                Toast.makeText(getApplicationContext(),  t.getCause().toString(), Toast.LENGTH_SHORT).show();
                                dialogFactory.createGenericErrorDialog(context," Votre compte n'est pas activé. Veuillez cliquer sur le lien qui vous à été envoyé par email pour l'activer" );

                            }
                        });

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_sign_in:
                submitForm();
                break;
        }
    }

    private class SignInTextWatcher implements TextWatcher {

        private View view;

        private SignInTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.et_email:
                    ETSUtils.validateEmail(mEtEmail, mLayoutEmail, SignInActivity.this);
                    break;
                case R.id.et_password:
                    ETSUtils.validateField(mEtPassword, mLayoutPassword, SignInActivity.this);
                    break;
            }
        }
    }
}
