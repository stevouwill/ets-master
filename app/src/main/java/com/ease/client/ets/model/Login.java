package com.ease.client.ets.model;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Login {

    @SerializedName("result")
    @Expose
    public boolean result;
    @SerializedName("value")
    @Expose
    public Value value;

    public Login() {
    }

    public boolean isResult() {
        return result;
    }

    public Value getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Login{" +
                "result=" + result +
                ", value=" + value +
                '}';
    }
}