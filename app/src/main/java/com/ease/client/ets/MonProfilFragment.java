package com.ease.client.ets;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MonProfilFragment.OnMonProfilFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MonProfilFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MonProfilFragment extends RootFragment implements View.OnClickListener{

    @BindView(R.id.btn_validate)
    Button mBtnValidate;
    @BindView(R.id.et_given_name)
    EditText mEtGivenName;
    @BindView(R.id.et_family_name)
    EditText mEtFamilyName;
    @BindView(R.id.et_email)
    EditText mEtEmail;
    @BindView(R.id.et_phone)
    EditText mEtPhoneNumber;
    @BindView(R.id.et_password)
    EditText mEtPassword;
    @BindView(R.id.et_repeat_password)
    EditText mEtRepeatPassword;
    @BindView(R.id.text_input_layout_family_name)
    TextInputLayout mLayoutFamilyName;
    @BindView(R.id.text_input_layout_given_name)
    TextInputLayout mLayoutGivenName;
    @BindView(R.id.text_input_layout_email)
    TextInputLayout mLayoutEmail;
    @BindView(R.id.text_input_layout_phone)
    TextInputLayout mLayoutPhoneNumber;
    @BindView(R.id.text_input_layout_password)
    TextInputLayout mLayoutPassword;
    @BindView(R.id.text_input_layout_repeat_password)
    TextInputLayout mLayoutRepeatPassword;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;

    private OnMonProfilFragmentInteractionListener mListener;

    public MonProfilFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
//     * @param param1 Parameter 1.
//     * @param param2 Parameter 2.
     * @return A new instance of fragment MonProfilFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MonProfilFragment newInstance(/*String param1, String param2*/) {
        MonProfilFragment fragment = new MonProfilFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mon_profil, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        ButterKnife.bind(this, view);
        ETSUtils.hideSoftKeyboard(getActivity());

        mEtGivenName.addTextChangedListener(new MonProfilTextWatcher(mEtGivenName));
        mEtFamilyName.addTextChangedListener(new MonProfilTextWatcher(mEtFamilyName));
        mEtEmail.addTextChangedListener(new MonProfilTextWatcher(mEtEmail));
        mEtPhoneNumber.addTextChangedListener(new MonProfilTextWatcher(mEtPhoneNumber));
        mEtPassword.addTextChangedListener(new MonProfilTextWatcher(mEtPassword));
        mEtRepeatPassword.addTextChangedListener(new MonProfilTextWatcher(mEtRepeatPassword));
        mBtnValidate.setOnClickListener(this);

        super.onViewCreated(view, savedInstanceState);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onFragmentInteraction();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMonProfilFragmentInteractionListener) {
            mListener = (OnMonProfilFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMonProfilFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_validate:
                submitForm();
                break;
        }
    }

    /**
     * Validating form
     */
    private void submitForm() {

        if (!ETSUtils.validateField(mEtGivenName, mLayoutGivenName, getActivity())) {
            return;
        } else {
            mLayoutGivenName.setError(null);
            mLayoutGivenName.setErrorEnabled(false);
        }

        if (!ETSUtils.validateField(mEtFamilyName, mLayoutFamilyName, getActivity())) {
            return;
        } else {
            mLayoutFamilyName.setError(null);
            mLayoutFamilyName.setErrorEnabled(false);
        }

        if (!ETSUtils.validateEmail(mEtEmail, mLayoutEmail, getActivity())) {
            return;
        } else {
            mLayoutEmail.setError(null);
            mLayoutEmail.setErrorEnabled(false);
        }

        if (!ETSUtils.validateField(mEtPhoneNumber, mLayoutPhoneNumber, getActivity())) {
            return;
        } else {
            mLayoutPhoneNumber.setError(null);
            mLayoutPhoneNumber.setErrorEnabled(false);
        }

        if (!ETSUtils.validateField(mEtPassword, mLayoutPassword, getActivity())) {
            return;
        } else {
            mLayoutPassword.setError(null);
            mLayoutPassword.setErrorEnabled(false);
        }

        if (!ETSUtils.validateField(mEtRepeatPassword, mLayoutRepeatPassword, getActivity())) {
            return;
        } else {
            mLayoutRepeatPassword.setError(null);
            mLayoutRepeatPassword.setErrorEnabled(false);
        }

        if (!mEtPassword.getText().toString().equals(mEtRepeatPassword.getText().toString())) {
            Toast.makeText(getActivity(), "Password mismatch!", Toast.LENGTH_SHORT).show();
            return;
        }

        Toast.makeText(getActivity(), "Thank You!", Toast.LENGTH_SHORT).show();
    }

    private class MonProfilTextWatcher implements TextWatcher {

        private View view;

        private MonProfilTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.et_given_name:
                    ETSUtils.validateField(mEtGivenName, mLayoutGivenName, getActivity());
                    break;
                case R.id.et_family_name:
                    ETSUtils.validateField(mEtFamilyName, mLayoutFamilyName, getActivity());
                    break;
                case R.id.et_email:
                    ETSUtils.validateEmail(mEtEmail, mLayoutEmail, getActivity());
                    break;
                case R.id.et_phone:
                    ETSUtils.validateField(mEtPhoneNumber, mLayoutPhoneNumber, getActivity());
                    break;
                case R.id.et_password:
                    ETSUtils.validateField(mEtPassword, mLayoutPassword, getActivity());
                    break;
                case R.id.et_repeat_password:
                    ETSUtils.validateField(mEtRepeatPassword, mLayoutRepeatPassword, getActivity());
                    break;
            }
        }
    }
}
