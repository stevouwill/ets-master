package com.ease.client.ets;

/**
 * Created by tin on 5/13/16.
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ease.client.ets.model.Model1;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
public class MesCoursesAdapter extends BaseRecyclerViewAdapter<MesCoursesAdapter.ViewHolder> {

    private static final String TAG = MesCoursesAdapter.class.getSimpleName();
    private ArrayList<Model1> mDataSet;
    private Context mContext;

    private MesCourseCallback mCallback;

    public interface MesCourseCallback{
        void onRideClick();
    }

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.txt3)
        TextView txt3;
        @BindView(R.id.txt4)
        TextView txt4;

        public ViewHolder(View v) {
            super(v);

            ButterKnife.bind(this, v);
            // Define click listener for the ViewHolder's View.
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.d(TAG, "Element " + getPosition() + " clicked.");
            mCallback.onRideClick();
        }
    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    public MesCoursesAdapter(ArrayList<Model1> dataSet, Context context, Fragment fragment) {
        mDataSet = dataSet;
        mContext = context;
        mCallback = (MesCourseCallback)fragment;
    }

    public void notifyAdapter(ArrayList<Model1> dataSet) {
        this.mDataSet = dataSet;
        notifyDataSetChanged();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_mes_courses_row, viewGroup, false);

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
        Model1 model = mDataSet.get(position);

        viewHolder.txt3.setText(model.text3);
        viewHolder.txt4.setText(model.text4);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataSet.size();
    }


}
