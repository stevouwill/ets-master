package com.ease.client.ets;

import android.app.Activity;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

/**
 * Created by Shazi on 20-Aug-16.
 */
public abstract class BaseActivity extends AppCompatActivity{

    public void addFragment(FragmentManager fm, int resourceId, Fragment fragment, String tag) {
        if (fm.findFragmentByTag(tag) == null) {
            fm.beginTransaction().add(resourceId, fragment, tag)
                    .commit();
        }
    }

    public void replaceFragment(FragmentManager fm, int resourceId, Fragment fragment, String tag, boolean addToBackStack) {
        if (addToBackStack) {
            fm.beginTransaction().replace(resourceId, fragment, tag)
                    .addToBackStack(null)
                    .commit();
        } else {
            fm.beginTransaction().replace(resourceId, fragment, tag)
                    .commit();
        }

    }
}
