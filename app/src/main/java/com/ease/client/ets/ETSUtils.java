package com.ease.client.ets;

import android.app.Activity;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

/**
 * Created by Shazi on 19-Aug-16.
 */
public class ETSUtils {

    public static void hideSoftKeyboard(Activity activity) {
        activity.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public static boolean validateEmail(EditText et, TextInputLayout layoutEt, Activity activity) {
        String email = et.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            layoutEt.setError(activity.getString(R.string.required));
            requestFocus(et, activity);
            return false;
        } else {
            layoutEt.setError(null);
            layoutEt.setErrorEnabled(false);
        }

        return true;
    }

    public static boolean validateField(EditText et, TextInputLayout layoutEt, Activity activity) {
        if (et.getText().toString().trim().isEmpty()) {
            layoutEt.setError(activity.getString(R.string.required));
            requestFocus(et, activity);
            return false;
        } else {
            layoutEt.setError(null);
            layoutEt.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private static void requestFocus(View view, Activity activity) {
        if (view.requestFocus()) {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

}
