package com.ease.client.ets;

/**
 * Created by Shazi on 21-Aug-16.
 */
public class ETSConstants {

    public final static String TAG_MON_PROFIL = "1000";
    public final static String TAG_MES_COURSE = "1001";
    public final static String TAG_LOCATION_HORAIRE = "1002";
    public final static String TAG_COURSE = "1003";
    public final static String TAG_MAP = "1004";
    public final static String TAG_RESERVE = "1005";
}
