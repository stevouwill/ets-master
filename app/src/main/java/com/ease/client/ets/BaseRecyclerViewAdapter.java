package com.ease.client.ets;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by tin on 6/3/16.
 */
public abstract class BaseRecyclerViewAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    @Override
    public abstract VH onCreateViewHolder(ViewGroup parent, int viewType);

    @Override
    public abstract void onBindViewHolder(VH viewHolder, final int position);

}
