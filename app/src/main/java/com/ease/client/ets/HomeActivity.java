package com.ease.client.ets;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class HomeActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        MesCourseFragment.OnMesCourseFragmentInteractionListener,
        MonProfilFragment.OnMonProfilFragmentInteractionListener,
        LocationHoraireFragment.OnLocationHoraireFragmentInteractionListener,
        ReserveUneCourseFragment.OnCourseFragmentInteractionListener{

    public static void showHomeScreen(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(true);
        setTitle("Reserve Une Course");
        replaceFragment(getSupportFragmentManager(), R.id.fragment_container, ReserveUneCourseFragment.newInstance(), ETSConstants.TAG_RESERVE, false);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_reserve_une_course) {
            this.setTitle("Reserve Une Course");
            replaceFragment(getSupportFragmentManager(), R.id.fragment_container, ReserveUneCourseFragment.newInstance(), ETSConstants.TAG_RESERVE, false);
        } else if (id == R.id.nav_location_horaire) {
            this.setTitle("Location Horaire");
            replaceFragment(getSupportFragmentManager(), R.id.fragment_container, LocationHoraireFragment.newInstance(), ETSConstants.TAG_LOCATION_HORAIRE, false);
        } else if (id == R.id.nav_mes_courses) {
            this.setTitle("Mes Courses");
            replaceFragment(getSupportFragmentManager(), R.id.fragment_container, MesCourseFragment.newInstance(), ETSConstants.TAG_MES_COURSE, false);
        } else if (id == R.id.nav_parameters) {
            this.setTitle("Parameters");
        } else if (id == R.id.nav_mon_profil) {
            this.setTitle("Mon Profil");
            replaceFragment(getSupportFragmentManager(), R.id.fragment_container, MonProfilFragment.newInstance(), ETSConstants.TAG_MON_PROFIL, false);
        } else if (id == R.id.nav_a_propose) {
            this.setTitle("A Propose");
        } else if (id == R.id.nav_deconnexion) {
            SignInActivity.signIn(this);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction() {

    }
}
