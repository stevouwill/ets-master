package com.ease.client.ets.model;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class RestError {

    @SerializedName("result")
    @Expose
    public boolean result;
    @SerializedName("value")
    @Expose
    public String value;
    @SerializedName("code")
    @Expose
    public int code;

    public RestError() {
    }

    public boolean isResult() {
        return result;
    }

    public String getValue() {
        return value;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "Error{" +
                "result=" + result +
                ", value='" + value + '\'' +
                ", code=" + code +
                '}';
    }
}