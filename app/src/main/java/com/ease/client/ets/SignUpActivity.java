package com.ease.client.ets;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignUpActivity extends BaseActivity implements View.OnClickListener{

    @BindView(R.id.txt_title)
    TextView mTxtTitle;
    @BindView(R.id.btn_validate)
    Button mBtnValidate;
    @BindView(R.id.et_given_name)
    EditText mEtGivenName;
    @BindView(R.id.et_family_name)
    EditText mEtFamilyName;
    @BindView(R.id.et_email)
    EditText mEtEmail;
    @BindView(R.id.et_phone)
    EditText mEtPhoneNumber;
    @BindView(R.id.et_password)
    EditText mEtPassword;
    @BindView(R.id.et_repeat_password)
    EditText mEtRepeatPassword;
    @BindView(R.id.text_input_layout_family_name)
    TextInputLayout mLayoutFamilyName;
    @BindView(R.id.text_input_layout_given_name)
    TextInputLayout mLayoutGivenName;
    @BindView(R.id.text_input_layout_email)
    TextInputLayout mLayoutEmail;
    @BindView(R.id.text_input_layout_phone)
    TextInputLayout mLayoutPhoneNumber;
    @BindView(R.id.text_input_layout_password)
    TextInputLayout mLayoutPassword;
    @BindView(R.id.text_input_layout_repeat_password)
    TextInputLayout mLayoutRepeatPassword;

    public static void createAccount(Context context) {
        Intent intent = new Intent(context, SignUpActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ButterKnife.bind(this);
        ETSUtils.hideSoftKeyboard(this);

        mEtGivenName.addTextChangedListener(new SignUpTextWatcher(mEtGivenName));
        mEtFamilyName.addTextChangedListener(new SignUpTextWatcher(mEtFamilyName));
        mEtEmail.addTextChangedListener(new SignUpTextWatcher(mEtEmail));
        mEtPhoneNumber.addTextChangedListener(new SignUpTextWatcher(mEtPhoneNumber));
        mEtPassword.addTextChangedListener(new SignUpTextWatcher(mEtPassword));
        mEtRepeatPassword.addTextChangedListener(new SignUpTextWatcher(mEtRepeatPassword));
        mBtnValidate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_validate:
                submitForm();
                break;
        }
    }

    /**
     * Validating form
     */
    private void submitForm() {

        if (!ETSUtils.validateField(mEtGivenName, mLayoutGivenName, this)) {
            return;
        } else {
            mLayoutGivenName.setError(null);
            mLayoutGivenName.setErrorEnabled(false);
        }

        if (!ETSUtils.validateField(mEtFamilyName, mLayoutFamilyName, this)) {
            return;
        } else {
            mLayoutFamilyName.setError(null);
            mLayoutFamilyName.setErrorEnabled(false);
        }

        if (!ETSUtils.validateEmail(mEtEmail, mLayoutEmail, this)) {
            return;
        } else {
            mLayoutEmail.setError(null);
            mLayoutEmail.setErrorEnabled(false);
        }

        if (!ETSUtils.validateField(mEtPhoneNumber, mLayoutPhoneNumber, this)) {
            return;
        } else {
            mLayoutPhoneNumber.setError(null);
            mLayoutPhoneNumber.setErrorEnabled(false);
        }

        if (!ETSUtils.validateField(mEtPassword, mLayoutPassword, this)) {
            return;
        } else {
            mLayoutPassword.setError(null);
            mLayoutPassword.setErrorEnabled(false);
        }

        if (!ETSUtils.validateField(mEtRepeatPassword, mLayoutRepeatPassword, this)) {
            return;
        } else {
            mLayoutRepeatPassword.setError(null);
            mLayoutRepeatPassword.setErrorEnabled(false);
        }

        if (!mEtPassword.getText().toString().equals(mEtRepeatPassword.getText().toString())) {
            Toast.makeText(getApplicationContext(), "Password mismatch!", Toast.LENGTH_SHORT).show();
            return;
        }

        Toast.makeText(getApplicationContext(), "Thank You!", Toast.LENGTH_SHORT).show();
        finish();
    }

    private class SignUpTextWatcher implements TextWatcher {

        private View view;

        private SignUpTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.et_given_name:
                    ETSUtils.validateField(mEtGivenName, mLayoutGivenName, SignUpActivity.this);
                    break;
                case R.id.et_family_name:
                    ETSUtils.validateField(mEtFamilyName, mLayoutFamilyName, SignUpActivity.this);
                    break;
                case R.id.et_email:
                    ETSUtils.validateEmail(mEtEmail, mLayoutEmail, SignUpActivity.this);
                    break;
                case R.id.et_phone:
                    ETSUtils.validateField(mEtPhoneNumber, mLayoutPhoneNumber, SignUpActivity.this);
                    break;
                case R.id.et_password:
                    ETSUtils.validateField(mEtPassword, mLayoutPassword, SignUpActivity.this);
                    break;
                case R.id.et_repeat_password:
                    ETSUtils.validateField(mEtRepeatPassword, mLayoutRepeatPassword, SignUpActivity.this);
                    break;
            }
        }
    }
}
