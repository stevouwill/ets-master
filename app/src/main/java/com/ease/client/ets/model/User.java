package com.ease.client.ets.model;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class User {

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("roles")
    @Expose
    public List<String> roles = new ArrayList<String>();
    @SerializedName("roles_id")
    @Expose
    public String rolesId;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("surname")
    @Expose
    public String surname;
    @SerializedName("corporation")
    @Expose
    public String corporation;
    @SerializedName("corporation_id")
    @Expose
    public String corporationId;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("reputation")
    @Expose
    public int reputation;

    public User() {
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public List<String> getRoles() {
        return roles;
    }

    public String getRolesId() {
        return rolesId;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getSurname() {
        return surname;
    }

    public String getCorporation() {
        return corporation;
    }

    public String getCorporationId() {
        return corporationId;
    }

    public String getTitle() {
        return title;
    }

    public int getReputation() {
        return reputation;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", roles=" + roles +
                ", rolesId='" + rolesId + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", surname='" + surname + '\'' +
                ", corporation='" + corporation + '\'' +
                ", corporationId='" + corporationId + '\'' +
                ", title='" + title + '\'' +
                ", reputation=" + reputation +
                '}';
    }
}