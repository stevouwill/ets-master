// Generated code from Butter Knife. Do not modify!
package com.ease.client.ets;

import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.Object;
import java.lang.Override;

public final class MesCoursesAdapter$ViewHolder_ViewBinder implements ViewBinder<MesCoursesAdapter.ViewHolder> {
  @Override
  public Unbinder bind(Finder finder, MesCoursesAdapter.ViewHolder target, Object source) {
    return new MesCoursesAdapter$ViewHolder_ViewBinding<>(target, finder, source);
  }
}
