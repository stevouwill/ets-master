// Generated code from Butter Knife. Do not modify!
package com.ease.client.ets;

import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class SignUpActivity_ViewBinding<T extends SignUpActivity> implements Unbinder {
  protected T target;

  public SignUpActivity_ViewBinding(T target, Finder finder, Object source) {
    this.target = target;

    target.mTxtTitle = finder.findRequiredViewAsType(source, R.id.txt_title, "field 'mTxtTitle'", TextView.class);
    target.mBtnValidate = finder.findRequiredViewAsType(source, R.id.btn_validate, "field 'mBtnValidate'", Button.class);
    target.mEtGivenName = finder.findRequiredViewAsType(source, R.id.et_given_name, "field 'mEtGivenName'", EditText.class);
    target.mEtFamilyName = finder.findRequiredViewAsType(source, R.id.et_family_name, "field 'mEtFamilyName'", EditText.class);
    target.mEtEmail = finder.findRequiredViewAsType(source, R.id.et_email, "field 'mEtEmail'", EditText.class);
    target.mEtPhoneNumber = finder.findRequiredViewAsType(source, R.id.et_phone, "field 'mEtPhoneNumber'", EditText.class);
    target.mEtPassword = finder.findRequiredViewAsType(source, R.id.et_password, "field 'mEtPassword'", EditText.class);
    target.mEtRepeatPassword = finder.findRequiredViewAsType(source, R.id.et_repeat_password, "field 'mEtRepeatPassword'", EditText.class);
    target.mLayoutFamilyName = finder.findRequiredViewAsType(source, R.id.text_input_layout_family_name, "field 'mLayoutFamilyName'", TextInputLayout.class);
    target.mLayoutGivenName = finder.findRequiredViewAsType(source, R.id.text_input_layout_given_name, "field 'mLayoutGivenName'", TextInputLayout.class);
    target.mLayoutEmail = finder.findRequiredViewAsType(source, R.id.text_input_layout_email, "field 'mLayoutEmail'", TextInputLayout.class);
    target.mLayoutPhoneNumber = finder.findRequiredViewAsType(source, R.id.text_input_layout_phone, "field 'mLayoutPhoneNumber'", TextInputLayout.class);
    target.mLayoutPassword = finder.findRequiredViewAsType(source, R.id.text_input_layout_password, "field 'mLayoutPassword'", TextInputLayout.class);
    target.mLayoutRepeatPassword = finder.findRequiredViewAsType(source, R.id.text_input_layout_repeat_password, "field 'mLayoutRepeatPassword'", TextInputLayout.class);
  }

  @Override
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mTxtTitle = null;
    target.mBtnValidate = null;
    target.mEtGivenName = null;
    target.mEtFamilyName = null;
    target.mEtEmail = null;
    target.mEtPhoneNumber = null;
    target.mEtPassword = null;
    target.mEtRepeatPassword = null;
    target.mLayoutFamilyName = null;
    target.mLayoutGivenName = null;
    target.mLayoutEmail = null;
    target.mLayoutPhoneNumber = null;
    target.mLayoutPassword = null;
    target.mLayoutRepeatPassword = null;

    this.target = null;
  }
}
