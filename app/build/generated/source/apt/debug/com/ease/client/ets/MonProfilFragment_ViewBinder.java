// Generated code from Butter Knife. Do not modify!
package com.ease.client.ets;

import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.Object;
import java.lang.Override;

public final class MonProfilFragment_ViewBinder implements ViewBinder<MonProfilFragment> {
  @Override
  public Unbinder bind(Finder finder, MonProfilFragment target, Object source) {
    return new MonProfilFragment_ViewBinding<>(target, finder, source);
  }
}
