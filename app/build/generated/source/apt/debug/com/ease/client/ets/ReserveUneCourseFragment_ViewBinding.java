// Generated code from Butter Knife. Do not modify!
package com.ease.client.ets;

import android.support.v7.widget.SearchView;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class ReserveUneCourseFragment_ViewBinding<T extends ReserveUneCourseFragment> implements Unbinder {
  protected T target;

  public ReserveUneCourseFragment_ViewBinding(T target, Finder finder, Object source) {
    this.target = target;

    target.mSearchView = finder.findRequiredViewAsType(source, R.id.sv_destination, "field 'mSearchView'", SearchView.class);
    target.mBtnReserve = finder.findRequiredViewAsType(source, R.id.btn_reserve_la_course, "field 'mBtnReserve'", Button.class);
  }

  @Override
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mSearchView = null;
    target.mBtnReserve = null;

    this.target = null;
  }
}
