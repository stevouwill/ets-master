// Generated code from Butter Knife. Do not modify!
package com.ease.client.ets;

import android.support.design.widget.TabLayout;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class LocationHoraireFragment_ViewBinding<T extends LocationHoraireFragment> implements Unbinder {
  protected T target;

  public LocationHoraireFragment_ViewBinding(T target, Finder finder, Object source) {
    this.target = target;

    target.mTabLayout = finder.findRequiredViewAsType(source, R.id.tabs, "field 'mTabLayout'", TabLayout.class);
    target.mViewPager = finder.findRequiredViewAsType(source, R.id.viewpager, "field 'mViewPager'", NonSwipeableViewPager.class);
  }

  @Override
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mTabLayout = null;
    target.mViewPager = null;

    this.target = null;
  }
}
