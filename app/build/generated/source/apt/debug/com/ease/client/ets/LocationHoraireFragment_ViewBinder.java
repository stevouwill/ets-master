// Generated code from Butter Knife. Do not modify!
package com.ease.client.ets;

import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.Object;
import java.lang.Override;

public final class LocationHoraireFragment_ViewBinder implements ViewBinder<LocationHoraireFragment> {
  @Override
  public Unbinder bind(Finder finder, LocationHoraireFragment target, Object source) {
    return new LocationHoraireFragment_ViewBinding<>(target, finder, source);
  }
}
