// Generated code from Butter Knife. Do not modify!
package com.ease.client.ets;

import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class CourseFragment_ViewBinding<T extends CourseFragment> implements Unbinder {
  protected T target;

  public CourseFragment_ViewBinding(T target, Finder finder, Object source) {
    this.target = target;

    target.mLyDate = finder.findRequiredViewAsType(source, R.id.ly_date, "field 'mLyDate'", RelativeLayout.class);
    target.mLyTime = finder.findRequiredViewAsType(source, R.id.ly_time, "field 'mLyTime'", RelativeLayout.class);
  }

  @Override
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mLyDate = null;
    target.mLyTime = null;

    this.target = null;
  }
}
