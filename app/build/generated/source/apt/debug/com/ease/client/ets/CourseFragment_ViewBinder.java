// Generated code from Butter Knife. Do not modify!
package com.ease.client.ets;

import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.Object;
import java.lang.Override;

public final class CourseFragment_ViewBinder implements ViewBinder<CourseFragment> {
  @Override
  public Unbinder bind(Finder finder, CourseFragment target, Object source) {
    return new CourseFragment_ViewBinding<>(target, finder, source);
  }
}
