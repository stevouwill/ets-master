// Generated code from Butter Knife. Do not modify!
package com.ease.client.ets;

import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class MesCoursesAdapter$ViewHolder_ViewBinding<T extends MesCoursesAdapter.ViewHolder> implements Unbinder {
  protected T target;

  public MesCoursesAdapter$ViewHolder_ViewBinding(T target, Finder finder, Object source) {
    this.target = target;

    target.txt3 = finder.findRequiredViewAsType(source, R.id.txt3, "field 'txt3'", TextView.class);
    target.txt4 = finder.findRequiredViewAsType(source, R.id.txt4, "field 'txt4'", TextView.class);
  }

  @Override
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.txt3 = null;
    target.txt4 = null;

    this.target = null;
  }
}
