// Generated code from Butter Knife. Do not modify!
package com.ease.client.ets;

import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.Object;
import java.lang.Override;

public final class ReserveUneCourseFragment_ViewBinder implements ViewBinder<ReserveUneCourseFragment> {
  @Override
  public Unbinder bind(Finder finder, ReserveUneCourseFragment target, Object source) {
    return new ReserveUneCourseFragment_ViewBinding<>(target, finder, source);
  }
}
