// Generated code from Butter Knife. Do not modify!
package com.ease.client.ets;

import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.Object;
import java.lang.Override;

public final class SignUpActivity_ViewBinder implements ViewBinder<SignUpActivity> {
  @Override
  public Unbinder bind(Finder finder, SignUpActivity target, Object source) {
    return new SignUpActivity_ViewBinding<>(target, finder, source);
  }
}
