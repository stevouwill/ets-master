// Generated code from Butter Knife. Do not modify!
package com.ease.client.ets;

import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.Object;
import java.lang.Override;

public final class MesCourseFragment_ViewBinder implements ViewBinder<MesCourseFragment> {
  @Override
  public Unbinder bind(Finder finder, MesCourseFragment target, Object source) {
    return new MesCourseFragment_ViewBinding<>(target, finder, source);
  }
}
