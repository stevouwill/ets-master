// Generated code from Butter Knife. Do not modify!
package com.ease.client.ets;

import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Finder;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class SignInActivity_ViewBinding<T extends SignInActivity> implements Unbinder {
  protected T target;

  public SignInActivity_ViewBinding(T target, Finder finder, Object source) {
    this.target = target;

    target.mEtEmail = finder.findRequiredViewAsType(source, R.id.et_email, "field 'mEtEmail'", EditText.class);
    target.mEtPassword = finder.findRequiredViewAsType(source, R.id.et_password, "field 'mEtPassword'", EditText.class);
    target.mBtnSignIn = finder.findRequiredViewAsType(source, R.id.btn_sign_in, "field 'mBtnSignIn'", Button.class);
    target.mTxtSignUp = finder.findRequiredViewAsType(source, R.id.txt_sign_up, "field 'mTxtSignUp'", TextView.class);
    target.mLayoutEmail = finder.findRequiredViewAsType(source, R.id.text_input_layout_email, "field 'mLayoutEmail'", TextInputLayout.class);
    target.mLayoutPassword = finder.findRequiredViewAsType(source, R.id.text_input_layout_password, "field 'mLayoutPassword'", TextInputLayout.class);
  }

  @Override
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mEtEmail = null;
    target.mEtPassword = null;
    target.mBtnSignIn = null;
    target.mTxtSignUp = null;
    target.mLayoutEmail = null;
    target.mLayoutPassword = null;

    this.target = null;
  }
}
